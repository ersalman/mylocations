### How do I get set up? ###

Open MyLocations.xcodeproj file and select MyLocation scheme from top and build and run.
if there is any issue or error related to library, select particular library scheme from top and build it then again, select project scheme and run.

To run on real device,
1. Add your developer account using Xcode -> Preference -> Account -> +
2. Click on project file Under general -> Signing -> Select your personal developer team account.

### Who do I talk to? ###

Salman Qureshi, er.salmanqureshi@gmail.com
