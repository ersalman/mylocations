
import CoreData
import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

  var window: UIWindow?
    
    var locationManager = CLLocationManager()
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
 
    var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    var placemark: CLPlacemark?
    var categoryName = "No Category"
    var date = Date()
    var descriptionText = ""
    
  lazy var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "DataModel")
    container.loadPersistentStores(completionHandler: { storeDescription, error in
      if let error = error {
        fatalError("Could load data store: \(error)")
      }
    })
    return container
  }()
  
  lazy var managedObjectContext: NSManagedObjectContext = self.persistentContainer.viewContext

    
    func doBackgroundTask() {
        
        DispatchQueue.main.async {
            
            self.beginBackgroundUpdateTask()
            
            self.startReceivingSignificantLocationChanges()
    
            self.endBackgroundUpdateTask()
            
        }
    }
    
    func beginBackgroundUpdateTask() {
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundUpdateTask()
        })
    }
    
    func endBackgroundUpdateTask() {
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskInvalid
    }
    
    func startReceivingSignificantLocationChanges() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != .authorizedAlways {
            // User has not authorized access to location information.
            return
        }
        
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            // The service is not available.
            return
        }
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            // Location updates are not authorized.
            manager.stopMonitoringSignificantLocationChanges()
            return
        }
        // Notify the user of any errors.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        coordinate = CLLocationCoordinate2DMake(manager.location!.coordinate.latitude, manager.location!.coordinate.longitude)
        print(manager.location!.coordinate.latitude)
        print(manager.location!.coordinate.longitude)
        
        lookUpCurrentLocation(manager: manager) { (placemark) in
            self.placemark = placemark
        }
        
        saveToCoreData()
    }
    
    func lookUpCurrentLocation(manager: CLLocationManager, completionHandler: @escaping (CLPlacemark?)
        -> Void ) {
        // Use the last reported location.
        if let lastLocation = manager.location {
            let geocoder = CLGeocoder()
            
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(lastLocation,
                                            completionHandler: { (placemarks, error) in
                                                if error == nil {
                                                    let firstLocation = placemarks?[0]
                                                    completionHandler(firstLocation)
                                                }
                                                else {
                                                    // An error occurred during geocoding.
                                                    completionHandler(nil)
                                                }
            })
        }
        else {
            // No location was available.
            completionHandler(nil)
        }
    }
    
    
    func saveToCoreData() {
       
        let location: Location
       
        location = Location(context: managedObjectContext)
        location.photoID = nil
        location.locationDescription = ""
        location.category = "No Category"
        location.latitude = coordinate.latitude
        location.longitude = coordinate.longitude
        location.date = Date()
        location.placemark = placemark
        
        do {
            try managedObjectContext.save()
        } catch {
            fatalCoreDataError(error)
        }
    }
    
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    customizeAppearance()
    
    print(applicationDocumentsDirectory)
    
    let tabBarController = window!.rootViewController as! UITabBarController
    
    if let tabBarViewControllers = tabBarController.viewControllers {
      let currentLocationViewController = tabBarViewControllers[0] as! CurrentLocationViewController
      currentLocationViewController.managedObjectContext = managedObjectContext

      let navigationController = tabBarViewControllers[1] as! UINavigationController
      let locationsViewController = navigationController.viewControllers[0] as! LocationsViewController
      locationsViewController.managedObjectContext = managedObjectContext
      
      let mapViewController = tabBarViewControllers[2] as! MapViewController
      mapViewController.managedObjectContext = managedObjectContext

      // Workaround for the Core Data bug.
      let _ = locationsViewController.view
    }
    
    listenForFatalCoreDataNotifications()
    return true
  }

  func customizeAppearance() {
    UINavigationBar.appearance().barTintColor = UIColor.black
    UINavigationBar.appearance().titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white ]
    
    UITabBar.appearance().barTintColor = UIColor.black
    
    let tintColor = UIColor(red: 255/255.0, green: 238/255.0,
                            blue: 136/255.0, alpha: 1.0)
    UITabBar.appearance().tintColor = tintColor
  }

  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    doBackgroundTask()
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }

  func listenForFatalCoreDataNotifications() {
    NotificationCenter.default.addObserver(forName: MyManagedObjectContextSaveDidFailNotification, object: nil, queue: OperationQueue.main, using: { notification in
      
      let alert = UIAlertController(
        title: "Internal Error",
        message: "There was a fatal error in the app and it cannot continue.\n\n"
               + "Press OK to terminate the app. Sorry for the inconvenience.",
        preferredStyle: .alert)
      
      let action = UIAlertAction(title: "OK", style: .default) { _ in
        let exception = NSException(name: NSExceptionName.internalInconsistencyException, reason: "Fatal Core Data error", userInfo: nil)
        exception.raise()
      }
      
      alert.addAction(action)
      
      self.viewControllerForShowingAlert().present(alert, animated: true,
                                                   completion: nil)
    })
  }
  
  func viewControllerForShowingAlert() -> UIViewController {
    let rootViewController = self.window!.rootViewController!
    if let presentedViewController = rootViewController.presentedViewController {
      return presentedViewController
    } else {
      return rootViewController
    }
  }
}
